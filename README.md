# MOB ORDER

This project is deployed on [https://mob.nov0.net](https://mob.nov0.net/) \
using [https://www.netlify.com](https://www.netlify.com/)


## Libraries

react - [https://www.reactjs.org](https://reactjs.org) \
google-spreadsheet - [link](https://www.npmjs.com/package/google-spreadsheet) - [documentation](https://theoephraim.github.io/node-google-spreadsheet/#/)

