export const isToday = (date) =>
    new Date(date).toDateString() === new Date().toDateString();